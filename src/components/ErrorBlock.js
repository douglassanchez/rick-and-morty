import React from 'react';

export default function ErrorBlock(props) {
  const error = props.error.response

  return (
    <h1 className="text-center text-uppercase">
      <p>{ `${error.status}: ${error.statusText}` }</p>
      <p>{ error.data.error }</p>
    </h1>
  );
}