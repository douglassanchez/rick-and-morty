import React from 'react';
import { Link } from 'react-router-dom';
import { Navbar } from 'react-bootstrap';

export default function NavbarBlock() {
  return (
    <Navbar 
      bg="dark" 
      variant="dark"
      sticky="top"
      className="Navbar"
    >
      <Navbar.Brand>
        <Link 
          to="/"
          className="ml-2 text-reset text-decoration-none font-weight-bold"
        >
          {' Home'}
        </Link>
      </Navbar.Brand>
    </Navbar>
  );
}