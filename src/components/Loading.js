import React, { Fragment } from 'react';
import { Spinner } from 'react-bootstrap';

export default function Loading() {
  return (
    <Fragment>
      <Spinner animation="grow" variant="primary" />
      <Spinner animation="grow" variant="secondary" />
      <Spinner animation="grow" variant="success" />
    </Fragment>
  );
}