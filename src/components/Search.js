import React, { useState } from 'react';
import { Form, Button } from 'react-bootstrap';

import './styles/Search.css';

export default function Search(props) {
  const [query, setQuery] = useState('');

  function onChange(event) {
    setQuery(event.target.value);
    props.onChange(query);
  }
  
  return (
    <div className="Search">
      <Form 
        className="Search__form"
        onSubmit={ e => e.preventDefault() }
      >
        <Form.Group>
          <Form.Control 
            type="text" 
            placeholder="Busca tu personaje favorito"
            value={query}
            onChange={onChange}
          >
          </Form.Control>
        </Form.Group>
      </Form>
    </div>
  );
}
