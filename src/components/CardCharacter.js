import React from 'react';
import { Link } from 'react-router-dom';
import { Card, Button } from 'react-bootstrap';

export default function CardCharacter(props) {
  const char = props.character;
  
  return (
    <Card style={{ width: '100%' }}>
      <Card.Img 
        variant="top" 
        src={char.image} 
      />
      <Card.Body>
        <Card.Title>{ char.name }</Card.Title>
        <Link to={`/characters/${char.id}`}>
          <Button variant="primary">Details</Button>
        </Link>
      </Card.Body>
    </Card>
  );
}