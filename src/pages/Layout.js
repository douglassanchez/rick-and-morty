import React, { Fragment } from 'react';

import NavbarBlock from '../components/Navbar';

export default function Layout(props) {
  return (
    <Fragment>
      <NavbarBlock />
      {props.children}
    </Fragment>
  );
}