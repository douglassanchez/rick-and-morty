import React from 'react';

function NotFound() {
  return <h1 className="text-center text-uppercase">404: Not Found</h1>;
}

export default NotFound;