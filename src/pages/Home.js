import React, { useState, useEffect, useRef } from 'react';
import { 
  Container, 
  Row, 
  Col,
} from 'react-bootstrap';

import Api from '../services/Api';
import './styles/Home.css';
import logo from '../images/logo.png'

import Search from '../components/Search';
import CardCharacter from '../components/CardCharacter';
import Loading from '../components/Loading';
import ErrorBlock from '../components/ErrorBlock';

import { useIntersectionObserver } from '../hooks/useIntersectionObserver';

const api = new Api();
api.createRepository({ name: 'character' });

function Home() {
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(null);
  const [pageCount, setPageCount] = useState(1);
  const [characters, setCharacters] = useState([]);
  
  async function getCharactersPage(numPage) {
    setLoading(true);
      
    const query = {
      page: numPage
    }

    try {
      const res = await api.repositories.character.index(query);

      setCharacters([...characters, ...res.data.results]);
      setLoading(false);

    } catch (err) {
      setLoading(false);
      setError(err);
    }
  }

  useEffect( () => {
    getCharactersPage(1);

  }, []);

  const root = useRef(null);
  const target = useRef();
  const [isThingIntersecting, setThingIntersecting] = useState(false);

  useIntersectionObserver({
    root,
    target,
    onIntersect: ([{ isIntersecting }]) => setThingIntersecting(isIntersecting)
  });

  useEffect(() => {
    if (isThingIntersecting) {
      setPageCount(pageCount + 1);

      if (pageCount > 1) { 
        getCharactersPage(pageCount);
      }
    }
  }, [isThingIntersecting]);

  const [term, setTerm] = useState('');
  function handleChange(term) {
    setTerm(term);
  }

  const filteredCharacters = characters.filter(character => {
    return character.name.toLowerCase().includes(term.toLowerCase().trim());
  });
  
  return (
    <Container 
      fluid
      className="px-0"
    >
      <Row className="mx-0">
        <Col 
          xs={12}
          md={4}
          className="d-flex flex-column justify-content-center align-items-center Container-search"
        >
          <img 
            src={logo} 
            alt="Logo Rick & Morty" 
            width="360px"
          />

          <Search onChange={handleChange} />
        </Col>

        <Col 
          xs={12}
          md={8}
        >
          <h3 className="mb-1 py-1 ml-2 font-weight-normal">Personajes de la serie</h3>
          <hr className="my-1" />
          
          <Container>
            <Row>
              {filteredCharacters.map(char => {
                return (
                  <Col
                    xs={12} 
                    md={4} 
                    className="my-2"
                    key={char.id}
                  >
                    <CardCharacter character={char} />
                  </Col>
                );
              })}
            </Row>
          </Container>
          
          <div 
            ref={target}
            className="pa-4 text-center"
          >
            { !isThingIntersecting && <Loading /> }
          </div>

          <Container className="Container-characters">
            { characters.length > 0 && filteredCharacters.length === 0 && 
              <h3>No characters were found</h3>
            }
            
            { loading === true && 
                <Loading />
            }
            
            { error &&
              <ErrorBlock error={error}  />
            }
          </Container>
        </Col>
      </Row>
    </Container>
  );
  
}

export default Home;