import React, { useState, useEffect } from 'react';
import { 
  Container, 
  Row, 
  Col,
  Card,
  Image
} from 'react-bootstrap';

import Api from '../services/Api';
import ErrorBlock from '../components/ErrorBlock';
import Loading from '../components/Loading';

const api = new Api();
api.createRepository({ name: 'character' });

export default function CharactersDetails(props) {
  const [character, setCharacter] = useState({});
  const [originCharacter, setOriginCharacter] = useState({});
  const [cantEpisodes, setCantEpisodes] = useState(0);
  const [locationCharacter, setLocationCharacter] = useState({});
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(null);

  useEffect( () => {
    getByIdCharacter();
  }, []);

  async function getByIdCharacter() {
    setLoading(true);

    try {
      const res = await api.repositories.character.show({ id: props.match.params.id });
      setCharacter(res.data);
      setOriginCharacter(res.data.origin);
      setCantEpisodes(res.data.episode.length);
      setLocationCharacter(res.data.location);
      setLoading(false);

    } catch (err) {
      console.log(err)
      setLoading(false);
      setError(err);
    }
  }

  if (loading) {
    return (
      <Container 
        fluid
        className="mt-4 d-flex justify-content-center align-items-center"
        style={{ height: 'calc(100vh - 56px)'}}
      >
        <Loading />
      </Container>
    );
  }

    if (error) {
      return (
        <Container 
          fluid
          className="mt-4 d-flex justify-content-center align-items-center"
          style={{ height: 'calc(100vh - 56px)'}}
        >
          <ErrorBlock error={error} />
        </Container>
      );
    }

  return (
    <Container 
      fluid
      className="mt-4"
    >
      <Card>
        <Card.Header as="h5">{ character.name }</Card.Header>
        <Card.Body>
          <Card.Title>Estado: { character.status }</Card.Title>
          <Card.Body className="d-flex">
            <div>
              <Image 
                src={ character.image} 
                roundedCircle 
                thumbnail
                style={{ width: '180px' }}
              />
            </div>
            
            <div className="ml-4">
              <p><span className="font-weight-bold">Especie:</span> { character.species }</p>
              <p><span className="font-weight-bold">Género:</span> { character.gender }</p>
              <p><span className="font-weight-bold">Origen:</span> { originCharacter.name }</p>
              <p><span className="font-weight-bold">Lugar:</span> { locationCharacter.name }</p>
              <p><span className="font-weight-bold">Cantidad de episodios:</span> { cantEpisodes}</p>
            </div>
          </Card.Body>
        </Card.Body>
      </Card>
    </Container>
  );
}