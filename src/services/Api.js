import axios from 'axios';

axios.defaults.baseURL = 'https://rickandmortyapi.com/api';

class Api {
  constructor() {
    this.repositories = {};
  }

  createRepository(repository) {
    this.repositories[repository.name] = this.createBasicCRUD(repository);
  }

  createBasicCRUD({ name }) {
    let repositories = {};
    const resource = `/${name}`;

    repositories.index = (query) => axios.get(resource, { params: query });
    repositories.show = ({ id }) => axios.get(`${resource}/${id}`);
    repositories.create = ({ payload }) => axios.post(resource, payload);
    repositories.update = ({ id, payload }) => axios.patch(`${resource}/${id}`, payload);
    repositories.delete = ({ id }) => axios.delete(`${resource}/${id}`);

    return repositories;
  }
}

export default Api;
