import React from 'react';
import { 
  BrowserRouter as Router, 
  Route, 
  Switch 
} from 'react-router-dom';

import Layout from './pages/Layout';
import Home from './pages/Home';
import CharactersDetails from './pages/CharactersDetails';
import NotFound from './pages/NotFound';

function App() {
  return (
    <Router>
      <Layout>
        <Switch>
          <Route exact path="/" component={Home} />
          <Route exact path="/characters/:id" component={CharactersDetails} />
          <Route component={NotFound} />
        </Switch>
      </Layout>
    </Router>
  );
}

export default App;
